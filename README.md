**Movies App [SwiftUI]**

Displays movies from https://www.omdbapi.com/ using SwiftUI

---

## Features

- Search for movies by title
- Show details of a movie that is selected from the list