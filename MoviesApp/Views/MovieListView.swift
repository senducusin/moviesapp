//
//  MovieListView.swift
//  MoviesApp
//
//  Created by Jansen Ducusin on 5/17/21.
//

import SwiftUI

struct MovieListView: View {
    
    @ObservedObject private var viewModel: MovieListViewModel
    @State private var movieName: String = ""
    
    init(){
        viewModel = MovieListViewModel()
    }
    
    var body: some View {
        VStack {
            
            TextField("Search", text: $movieName, onEditingChanged: {_ in}, onCommit: {
                viewModel.searchByName(self.movieName)
            })
            .textFieldStyle(RoundedBorderTextFieldStyle())
            Spacer()
                
                .navigationBarTitle("Movies")
            
            if viewModel.loadingState == .success {
                MovieList(movies: viewModel.movies)
            }else if viewModel.loadingState == .failed {
                FailedView()
            }else if viewModel.loadingState == .loading {
                LoadingView()
            }
            
            
        }
        .embedNavigationView()
        .padding()
    }
}

struct MovieListView_Previews: PreviewProvider {
    static var previews: some View {
        MovieListView()
    }
}
