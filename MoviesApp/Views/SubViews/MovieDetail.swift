//
//  MovieDetail.swift
//  MoviesApp
//
//  Created by Jansen Ducusin on 5/17/21.
//

import SwiftUI

struct MovieDetail: View {
    
    let viewModel: MovieDetailViewModel
    
    var body: some View {
        ScrollView {
            
            VStack(alignment: .leading, spacing: 10){
                ImageUrl(url: viewModel.poster)
                    .cornerRadius(10)
                
                
                Text(viewModel.title)
                    .font(.title)
                
                Text(viewModel.plot)
                
                Text("Director")
                    .fontWeight(.bold)
                
                Text(viewModel.director)
                
                HStack {
                    Rating(rating: .constant(viewModel.rating))
                    
                    Text("\(viewModel.rating)/10")
                }.padding(.top, 10)
                
                Spacer()
                
            }.padding()
            
            .navigationBarTitle(viewModel.title)
        }
    }
}
