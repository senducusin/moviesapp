//
//  Rating.swift
//  MoviesApp
//
//  Created by Jansen Ducusin on 5/15/21.
//

import SwiftUI

struct Rating: View {
    
    @Binding var rating: Int?
    
    private func starType(index:Int) -> String {
        if let rating = rating {
            return index <= rating ? "star.fill" : "star"
        }else{
            return "star"
        }
    }
    
    var body: some View {
        
        HStack {
            ForEach(1...10, id: \.self) { index in
                Image(systemName: starType(index: index))
                    .foregroundColor(.orange)
                    .onTapGesture {
                        rating = index
                    }
            }
        }
        
    }
    
}

struct Rating_Previews: PreviewProvider {
    static var previews: some View {
        Rating(rating: .constant(8))
    }
}
