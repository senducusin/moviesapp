//
//  MovieList.swift
//  MoviesApp
//
//  Created by Jansen Ducusin on 5/17/21.
//

import SwiftUI

struct MovieList: View {
    
    let movies: [Movie]
    
    var body: some View {
        List(movies, id:\.imdbId) { movie in
            NavigationLink(
                destination: MovieDetailView(imdbId: movie.imdbId)) {
                MovieCell(movie: movie)
            }
          
        }
    }
}

struct MovieCell: View {
    
    let movie: Movie
    
    var body: some View {
        
        HStack(alignment: .top){
            ImageUrl(url: movie.poster)
                .frame(width: 80, height: 120)
                .cornerRadius(6)
            
            VStack(alignment: .leading){
                Text(movie.title)
                    .font(.headline)
                
                Text(movie.year)
                    .opacity(0.5)
                    .padding(.top, 10)
            }.padding(5)
            
            Spacer()
            
        }.contentShape(Rectangle())
        
    }
}
