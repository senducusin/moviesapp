//
//  MovieDetailView.swift
//  MoviesApp
//
//  Created by Jansen Ducusin on 5/17/21.
//

import SwiftUI

struct MovieDetailView: View {
    
    let imdbId: String
    @ObservedObject var viewModel = MovieDetailViewModel()
    
    var body: some View {
        VStack {
            if viewModel.loadingState == .loading {
                LoadingView()
            }else if viewModel.loadingState == .success {
                MovieDetail(viewModel: viewModel)
            }else if viewModel.loadingState == .failed {
                FailedView()
            }

        }
        .onAppear{
            viewModel.getDetailsByImdbId(imdbId: imdbId)
        }
        
    }
}
