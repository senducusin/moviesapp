//
//  FailedView.swift
//  MoviesApp
//
//  Created by Jansen Ducusin on 5/17/21.
//

import SwiftUI

struct FailedView: View {
    var body: some View{
        VStack {
            VStack {
                Image(uiImage: UIImage(systemName: "xmark.icloud")!)
                    .resizable()
                    .frame(width: 60, height: 40, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                Text("Oops!")
                    .font(.largeTitle)
                
                Text(" Title Not Found")
            }
            Spacer()
        }
    }
}

struct FailedView_Previews: PreviewProvider {
    static var previews: some View {
        FailedView()
    }
}
