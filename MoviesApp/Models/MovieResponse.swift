//
//  MovieResponse.swift
//  MoviesApp
//
//  Created by Jansen Ducusin on 5/15/21.
//

import Foundation

struct MovieResponse: Codable {
    let movies: [Movie]
    
    private enum CodingKeys: String, CodingKey {
        case movies = "Search"
    }
}

extension MovieResponse {
   static func getMoviesBy(keyword:String) -> Resource<MovieResponse>?{
        guard let url = URL(string: "http://omdbapi.com/?s=\(keyword)&apikey=\(WebService.API_KEY)") else {return nil}
        
        return Resource(url: url)
    }
}

