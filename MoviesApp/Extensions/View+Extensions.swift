//
//  View+Extensions.swift
//  MoviesApp
//
//  Created by Jansen Ducusin on 5/17/21.
//

import SwiftUI

extension View {
    func embedNavigationView() -> some View {
        return NavigationView {self}
    }
}
