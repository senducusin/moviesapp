//
//  String+Extensions.swift
//  MoviesApp
//
//  Created by Jansen Ducusin on 5/17/21.
//

import Foundation

extension String {
    func trimmedAndEscaped() -> String {
        let trimmedString = trimmingCharacters(in: .whitespacesAndNewlines)
        
        return trimmedString
            .addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? self
    }
}
