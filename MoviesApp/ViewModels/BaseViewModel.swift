//
//  BaseViewModel.swift
//  MoviesApp
//
//  Created by Jansen Ducusin on 5/17/21.
//

import Foundation

enum LoadingState {
    case loading, success, failed, none
}

class BaseViewModel: ObservableObject {
    @Published var loadingState: LoadingState = .none
}
