//
//  MovieListViewModel.swift
//  MoviesApp
//
//  Created by Jansen Ducusin on 5/17/21.
//

import Foundation

class MovieListViewModel: BaseViewModel {
    @Published var movies = [Movie]()
    
    func searchByName(_ name: String){
        
        let trimmedAndEscapedName = name.trimmedAndEscaped()
        
        guard !trimmedAndEscapedName.isEmpty,
              let resource = MovieResponse.getMoviesBy(keyword: trimmedAndEscapedName) else {return}
        
        DispatchQueue.main.async {
            self.loadingState = .loading
        }
        
        WebService.shared.load(resource: resource) { [weak self] result in
            
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    self?.movies = response.movies
                    self?.loadingState = .success
                }
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async {
                    self?.loadingState = .failed
                }
            }
            
        }
    }
    
    
}
