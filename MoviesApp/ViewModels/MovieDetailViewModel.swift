//
//  MovieDetailViewModel.swift
//  MoviesApp
//
//  Created by Jansen Ducusin on 5/17/21.
//

import Foundation

class MovieDetailViewModel: ObservableObject {
    private var movie: Movie?
    
    @Published var loadingState = LoadingState.loading
    
    init(movie: Movie? = nil) {
        self.movie = movie
    }
    
    func getDetailsByImdbId(imdbId:String){
        guard !imdbId.isEmpty,
              let resource = Movie.getMovieBy(id: imdbId) else {return}
        
        WebService.shared.load(resource: resource) { [weak self] result in
            switch result {
            
            case .success(let movie):
                DispatchQueue.main.async {
                    self?.movie = movie
                    self?.loadingState = .success
                }
               
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async {
                    self?.loadingState = .failed
                }
            }
        }
    }
    
    var title: String {
        movie?.title ?? ""
    }
    
    var poster: String {
        movie?.poster ?? ""
    }
    
    var plot: String {
        movie?.plot ?? ""
    }
    
    var rating: Int {
        get {
            let ratingAsDouble = Double(movie?.imdbRating ?? "0.0")
            return Int(ceil(ratingAsDouble ?? 0.0))
        }
    }
    
    var director: String{
        movie?.director ?? ""
    }
}
